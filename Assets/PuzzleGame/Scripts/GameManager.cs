﻿namespace InfinityPuzzle
{
    public static class GameManager
    {
        public static GameStatus gameStatus = GameStatus.Playing;
        public static int currentLevel = 0;
        public static int totalDiamonds; //Whenever a player wins, we reward them with a diamond.
    }

    public enum GameStatus
    {
        Playing,
        Complete
    }
}