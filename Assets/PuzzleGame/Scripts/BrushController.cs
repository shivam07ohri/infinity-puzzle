﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InfinityPuzzle
{
    public class BrushController : MonoBehaviour
    {
        [HideInInspector] public Vector2Int currentCoords;
    }
}